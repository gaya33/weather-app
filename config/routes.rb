Rails.application.routes.draw do
  root 'weather#index'
  get 'weather', to: 'weather#fetch_forecast'
  get 'weather_per_day', to: 'weather#forecast_per_day'
end
