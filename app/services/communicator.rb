class Communicator

  def initialize(options)
    @city = options[:city].try(:titleize)
  end

  def fetch_daily_forecast
    conn = Faraday.new(url: Settings.open_weather_app.url)
    response = conn.get 'daily' do |req|
      req.params = {
          q: @city,
          appid: Settings.open_weather_app.api_key,
          cnt: 16
      }
    end
    
    ActiveSupport::JSON.decode(response.body)
  end



end