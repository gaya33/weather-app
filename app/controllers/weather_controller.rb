class WeatherController < ApplicationController

  # root page
  def index
  end

  def fetch_forecast
    forecast = Communicator.new({
      city: weather_params[:q]
      }).fetch_daily_forecast
    render 'fetch_forecast', :locals =>{:data => forecast}
  end

  def forecast_per_day
    render 'forecast_per_day', :locals =>{:data => params}
  end

  private
    
    def weather_params
      params.permit(:q)
    end


end