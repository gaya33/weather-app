require 'rails_helper'

RSpec.describe WeatherController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end
  describe "GET #fetch_forecast" do

    it "renders the index template" do
      get :fetch_forecast
      expect(response).to render_template('fetch_forecast')
    end
    it "renders the index template with the correct data" do
      file = File.read('spec/weather.json')
      result = ActiveSupport::JSON.decode(file)
      get :fetch_forecast
      
      expect(response).to render_template('fetch_forecast', :locals => {data: result}) 
    end
  end

  describe "GET #forecast_per_day" do
    it "renders the index template" do
      get :forecast_per_day
      expect(response).to render_template('forecast_per_day')
    end
  end
end