require 'rails_helper'

RSpec.describe Communicator do
  context "initializing" do 
    context "when a call is made to the service" do
      it "uses the query param its called with" do
        options = {city: 'London'}
        communicator = Communicator.new(options)
        expect(communicator.as_json['city']).to eq(options[:city])
      end

      it "calls the openweather api and returns the expected data" do
        file = File.read('spec/weather.json')
        result = ActiveSupport::JSON.decode(file)
        communicator = Communicator.new({city: 'london'})

        allow(communicator).to receive(:fetch_daily_forecast).and_return(result)
        expect(communicator.fetch_daily_forecast).to eq(result)
      end
    end
  end
end